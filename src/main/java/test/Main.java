package test;

import com.ibm.watson.developer_cloud.http.ServiceCall;
import com.ibm.watson.developer_cloud.natural_language_classifier.v1.NaturalLanguageClassifier;
import com.ibm.watson.developer_cloud.natural_language_classifier.v1.model.Classification;
import com.ibm.watson.developer_cloud.natural_language_classifier.v1.model.Classifier;
import com.ibm.watson.developer_cloud.natural_language_classifier.v1.model.Classifiers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Paths;

public final class Main {

    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) throws InterruptedException {

        String username = "";
        String password = "";
        String classifierId = "";
        String text = "How hot will it be today?";
        boolean createClassifier = false;
        boolean delete = false;

        NaturalLanguageClassifier service = new NaturalLanguageClassifier();
        service.setUsernameAndPassword(username, password);

        Classifiers classifiers = service.getClassifiers().execute();
        logger.info("Classifiers: {}", classifiers);

        Classifier classifier;
        if (createClassifier) {
            ServiceCall<Classifier> serviceCall = service.createClassifier("name", "en", Paths.get("./weather_data_train.csv").toFile());
            classifier = serviceCall.execute();
        } else {
            classifier = service.getClassifier(classifierId).execute();
        }
        logger.info("Classifier: {}", classifier);

        while (true) {
            classifier = service.getClassifier(classifier.getId()).execute();
            Classifier.Status status = classifier.getStatus();
            if (status.equals(Classifier.Status.AVAILABLE)) break;
            Thread.sleep(10000);
            logger.info("Status: {}", status);
        }

        Classification classification = service.classify(classifier.getId(), text).execute();

        if (delete) {
            service.deleteClassifier(classifier.getId()).execute();
        }

        logger.info("Result: {}", classification);

    }

}
